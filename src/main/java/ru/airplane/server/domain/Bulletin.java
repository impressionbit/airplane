package ru.airplane.server.domain;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "BULLETIN") // бюллетени по доработкам самолета
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class Bulletin {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Integer id;

    @Column(name = "BULLETIN_NUMBER")
    private String bulletinNumber;

    @Column(name = "NAME")
    private String name;

    @Column(name = "TYPE_OF_AIRCRAFT")
    private String typeOfAircraft;

    @OneToMany
    HashSet <CompletenessOfTheBulletin> instanceBuiietin;

    @ManyToOne
    @JoinColumn (name = "katalog_id")
    private Katalog katalog;
}