package ru.airplane.server.domain;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import javax.persistence.*;

/*
 резинотехнические изделия
 */

@Entity
@Table(name = "RTP")
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class RTP {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Integer id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "TYPE_OF_AIRCRAFT")
    private String typeOfAircraft;

    /*
     вид замены: обязательная или по дефектации
     */
    @Column(name = "TYPE_OF_REPLACEMENT")
    private boolean typeOfReplacement;

    @Column(name = "OWN_PRODUCTION") // возможность собственного изготовления
    private boolean ownProduction;

    @ManyToOne
    @JoinColumn (name = "katalog_id")
    private Katalog katalog;
}
