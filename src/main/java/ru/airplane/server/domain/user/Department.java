package ru.airplane.server.domain.user;

import javax.persistence.*;
import java.util.Set;

/**
 * Класс пгрруппы пользователей
 */
@Entity
@Table(name = "DEPARTMENT")
public class Department {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "NAME", unique = true, nullable = false)
    private String name;

    @OneToMany
    private Set<User> users;


    public Department(String name) {
        this.name = name;
    }

    public Department() {
    }

    public Department setName(String name) {
        this.name = name;
        return this;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Set<User> getUsers() {
        return users;
    }

    public Department setId(Long id) {
        this.id = id;
        return this;
    }

    public Department setUsers(Set<User> users) {
        this.users = users;
        return this;
    }
}
