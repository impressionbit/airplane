package ru.airplane.server.domain.user;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import javax.persistence.*;
import java.util.Set;


/**
 * Класс пользователя
 */
@Entity
@Table(name = "USERS")
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "PASSWORD")
    private String password;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "user_role",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id")
    )
    private Set<Role> roles;

    @ManyToOne
    @JoinColumn(name="department_id", referencedColumnName = "id", nullable=true)
    private Department department;


    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "passport_id", referencedColumnName = "id")
    private Passport passport;


    public User() {}

    public User(String name) {
        this.name = name;
    }

    public void setRoles (Set<Role> roles){
        this.roles = roles;
    }

    public User setId(Long id) {
        this.id = id;
        return this;
    }

    public User setName(String name) {
        this.name = name;
        return this;
    }

    public User setPassword(String password) {
        this.password = password;
        return this;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPassword() {
        return password;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public Passport getPassport() {
        return passport;
    }

    public User setPassport(Passport passport) {
        this.passport = passport;
        return this;
    }

    public User setDepartment(Department department) {
        this.department = department;
        return this;
    }

    public Department getDepartment() {
        return department;
    }
}
