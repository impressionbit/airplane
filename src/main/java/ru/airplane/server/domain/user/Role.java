package ru.airplane.server.domain.user;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import org.springframework.data.rest.core.annotation.RestResource;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "ROLES")
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class Role {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "NAME", nullable = true, unique = true)
    private String name;

    @Column(name = "ROLE", nullable = true, unique = true)
    private String role;

    @ManyToMany
    @JoinTable(
            name = "user_role",
            joinColumns = @JoinColumn(name = "role_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id")
    )
    @RestResource(exported = false)
    private Set<User> users = new HashSet<>();

    public Role (String name, String role){
        this.name = name;
        this.role = role;
    }

    public Role() {
    }

    public Role setId(Long id) {
        this.id = id;
        return this;
    }

    public Role setName(String name) {
        this.name = name;
        return this;
    }

    public Role setRole(String role) {
        this.role = role;
        return this;
    }

    public Role setUsers(Set<User> users) {
        this.users = users;
        return this;
    }


    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getRole() {
        return role;
    }

    public Set<User> getUsers() {
        return users;
    }
}
