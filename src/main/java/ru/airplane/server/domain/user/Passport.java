package ru.airplane.server.domain.user;

import javax.persistence.*;

/**
 * Класс паспорта пользователя
 */
@Entity
@Table(name = "PASSPORT")
public class Passport {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "SERIES")
    private String series;

    @Column(name = "ADDRESS")
    private String address;

    public Passport() {
    }

    public Passport(String series, String address) {
        this.series = series;
        this.address = address;
    }


    public Passport setId(Long id) {
        this.id = id;
        return this;
    }

    public Passport setSeries(String series) {
        this.series = series;
        return this;
    }

    public Passport setAddress(String address) {
        this.address = address;
        return this;
    }
}
