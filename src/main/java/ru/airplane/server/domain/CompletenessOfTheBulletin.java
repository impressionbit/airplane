package ru.airplane.server.domain;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import javax.persistence.*;

@Entity
@Table(name = "COMPLETENESS_OF_THE_BULLETIN")// комплектность бюллетеня
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class CompletenessOfTheBulletin {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Integer id;

    @Column(name = "NAME_AGREGATS") // наименование блока или агрегата входящего в состав бюллетеня
    private String nameAgregats;

    @Column(name = "NAME_DETAILS") // наименование детали входящей в бюллетень / комплект монтажныйх частей
    private String nameDetails;

    @Column(name = "SPECIAL_TOOL") // наименование специального инструмента
    private String specialTool;

    @Column(name = "OWN_PRODUCTION") // возможность собственного изготовления
    private boolean ownProduction;

    @ManyToOne
    @JoinColumn (name = "bulletin_id")
    private Bulletin bulletin;
}
