package ru.airplane.server.domain;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import javax.persistence.*;

@Entity
@Table(name = "AGREGAT")
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class Agregat {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Integer id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "TYPE_OF_AIRCRAFT")
    private String typeOfAircraft;

    @ManyToOne
    @JoinColumn (name = "katalog_id")
    private Katalog katalog;
}
