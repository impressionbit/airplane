package ru.airplane.server.domain;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import javax.persistence.*;

@Entity
@Table(name = "DETAILS")// детали по чертежам РД
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class Details {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Integer id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "TYPE_OF_AIRCRAFT")
    private String typeOfAircraft;

    @Column(name = "TYPE_OF_REPLACEMENT") // вид замены: обязательная или по дефектации
    private boolean typeOfReplacement;

    @Column(name = "OWN_PRODUCTION") // возможность собственного изготовления
    private boolean ownProduction;

    @Column(name = "MATERIAL")
    private String material; //   материал изготолвнеия детали

    @ManyToOne
    @JoinColumn (name = "katalog_id")
    private Katalog katalog;
}
