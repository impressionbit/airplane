package ru.airplane.server.domain;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import javax.persistence.*;

@Entity
@Table(name = "COMPLETENESS_OF_THE_RGС")// комплектность ремонтного группового комплекта
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class CompletenessOfTheRgc {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Integer id;

    @Column(name = "TYPE_OF_AGREGAT")
    private String typeOfAgregat;

    @Column(name = "PART_NAME") // наименование детали входящей в ргк
    private String partName;

    @Column(name = "OWN_PRODUCTION") // возможность собственного изготовления
    private boolean ownProduction;

    @Column(name = "USAGE_SHARE") // доля использования в составе одного самолета
    private String usageShare;

    @ManyToOne
    @JoinColumn (name = "rgc_id")
    private RGC rgc;
}
