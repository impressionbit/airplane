package ru.airplane.server.domain;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "RGС")// ремонтный групповой комплект
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class RGC {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Integer id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "TYPE_OF_AIRCRAFT")
    private String typeOfAircraft;

    @Column(name = "TYPE_OF_AGREGAT")
    private String typeOfAgregat;

    @Column(name = "TYPE_OF_REPLACEMENT") // вид замены: обязательная или по дефектации
    private boolean typeOfReplacement;

    @OneToMany
    HashSet<CompletenessOfTheRgc> instanceRGC;

    @ManyToOne
    @JoinColumn (name = "katalog_id")
    private Katalog katalog;
}
