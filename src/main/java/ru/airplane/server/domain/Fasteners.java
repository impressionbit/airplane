package ru.airplane.server.domain;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import javax.persistence.*;

@Entity
@Table(name = "FASTENERS")// крепеж
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class Fasteners {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Integer id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "OWN_PRODUCTION") // возможность собственного изготовления
    private boolean ownProduction;

    @Column(name = "TYPE_OF_REPLACEMENT") // вид замены: обязательная или по дефектации
    private boolean typeOfReplacement;

    @ManyToOne
    @JoinColumn (name = "katalog_id")
    private Katalog katalog;
}
