package ru.airplane.server.domain;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;
import java.util.Set;
import java.util.TreeSet;

@Entity
@Table(name = "KATALOG")
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class Katalog {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Integer id;

    @Column(name = "TYPE_OF_AIRCRAFT")
    private String typeOfAircraft;

    @OneToMany(mappedBy="id")
    Set<Agregat> Agregats;

    @OneToMany(mappedBy="id")
    Set<Bearings> Bearing;

    @OneToMany(mappedBy="id")
    Set<Bulletin> Bulletins;

    @OneToMany(mappedBy="id")
    Set<Details> Detail;

    @OneToMany(mappedBy="id")
    Set<ERP> Erps;

    @OneToMany(mappedBy="id")
    Set<Fasteners> Fastener;

    @OneToMany(mappedBy="id")
    Set<RGC> Rgcs;

    @OneToMany(mappedBy="id")
    Set<RTP> Rtps;

    public Katalog (String typeOfAircraft) {
        this.typeOfAircraft = typeOfAircraft;
    }
    public Katalog () {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTypeOfAircraft() {
        return typeOfAircraft;
    }

    public void setTypeOfAircraft(String typeOfAircraft) {
        this.typeOfAircraft = typeOfAircraft;
    }

    public Set<Agregat> getAgregats() {
        return Agregats;
    }

    public void setAgregats(Set<Agregat> agregats) {
        Agregats = agregats;
    }

    public Set<Bearings> getBearing() {
        return Bearing;
    }

    public void setBearing(Set<Bearings> bearing) {
        Bearing = bearing;
    }

    public Set<Bulletin> getBulletins() {
        return Bulletins;
    }

    public void setBulletins(Set<Bulletin> bulletins) {
        Bulletins = bulletins;
    }

    public Set<Details> getDetail() {
        return Detail;
    }

    public void setDetail(Set<Details> detail) {
        Detail = detail;
    }

    public Set<ERP> getErps() {
        return Erps;
    }

    public void setErps(Set<ERP> erps) {
        Erps = erps;
    }

    public Set<Fasteners> getFastener() {
        return Fastener;
    }

    public void setFastener(Set<Fasteners> fastener) {
        Fastener = fastener;
    }

    public Set<RGC> getRgcs() {
        return Rgcs;
    }

    public void setRgcs(Set<RGC> rgcs) {
        Rgcs = rgcs;
    }

    public Set<RTP> getRtps() {
        return Rtps;
    }

    public void setRtps(Set<RTP> rtps) {
        Rtps = rtps;
    }
}
