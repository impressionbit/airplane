package ru.airplane.server.domain.repository.myrepository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.airplane.server.domain.Fasteners;
import ru.airplane.server.domain.RGC;

@Repository
public interface RgcRepository extends CrudRepository <RGC,Integer> {
}
