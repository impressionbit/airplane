package ru.airplane.server.domain.repository.myrepository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.airplane.server.domain.CompletenessOfTheBulletin;
import ru.airplane.server.domain.CompletenessOfTheRgc;

@Repository
public interface CompletenessOfTheRgcRepository extends CrudRepository <CompletenessOfTheRgc,Integer> {
}
