package ru.airplane.server.domain.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.airplane.server.domain.user.Role;
import ru.airplane.server.domain.user.User;

import java.util.Set;


@Repository
public interface UserRepository extends CrudRepository<User, Long> {
    <T> Set<T> findAllByName(Class<T> type, String name);
    <T> T findAllById (Class<T> type, long id);
}
