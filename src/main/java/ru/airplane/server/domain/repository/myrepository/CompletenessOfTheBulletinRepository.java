package ru.airplane.server.domain.repository.myrepository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.airplane.server.domain.Bulletin;
import ru.airplane.server.domain.CompletenessOfTheBulletin;

@Repository
public interface CompletenessOfTheBulletinRepository extends CrudRepository <CompletenessOfTheBulletin,Integer> {
}
