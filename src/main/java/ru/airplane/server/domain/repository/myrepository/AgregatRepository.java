package ru.airplane.server.domain.repository.myrepository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.airplane.server.domain.Agregat;
import ru.airplane.server.domain.Katalog;

@Repository
public interface AgregatRepository extends CrudRepository <Agregat,Integer> {
}
