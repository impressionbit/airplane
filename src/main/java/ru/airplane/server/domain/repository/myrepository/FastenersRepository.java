package ru.airplane.server.domain.repository.myrepository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.airplane.server.domain.ERP;
import ru.airplane.server.domain.Fasteners;

@Repository
public interface FastenersRepository extends CrudRepository <Fasteners,Integer> {
}
