package ru.airplane.server.domain.repository.myrepository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.airplane.server.domain.Agregat;
import ru.airplane.server.domain.Bulletin;

@Repository
public interface BulletinRepository extends CrudRepository <Bulletin,Integer> {
}
