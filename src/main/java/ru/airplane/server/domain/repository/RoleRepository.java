package ru.airplane.server.domain.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.airplane.server.domain.user.Role;


@Repository
public interface RoleRepository extends CrudRepository<Role, Long> {
}
