package ru.airplane.server.domain.repository.myrepository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.airplane.server.domain.Katalog;

import java.util.Set;

@Repository
public interface KatalogRepository extends CrudRepository <Katalog,Integer> {
    <T> Set <T> findAllByTypeOfAircraft (Class<T> type, String typeOfAircraft );
}
