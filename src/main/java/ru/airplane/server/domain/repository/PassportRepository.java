package ru.airplane.server.domain.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.airplane.server.domain.user.Passport;


@Repository
public interface PassportRepository extends CrudRepository<Passport, Long> {

}
