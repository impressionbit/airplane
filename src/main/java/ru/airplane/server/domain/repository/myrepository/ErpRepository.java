package ru.airplane.server.domain.repository.myrepository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.airplane.server.domain.Details;
import ru.airplane.server.domain.ERP;

@Repository
public interface ErpRepository extends CrudRepository <ERP,Integer> {
}
