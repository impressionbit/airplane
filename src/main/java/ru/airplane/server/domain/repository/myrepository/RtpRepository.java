package ru.airplane.server.domain.repository.myrepository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.airplane.server.domain.RGC;
import ru.airplane.server.domain.RTP;

@Repository
public interface RtpRepository extends CrudRepository <RTP,Integer> {
}
