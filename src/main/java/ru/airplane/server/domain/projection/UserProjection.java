package ru.airplane.server.domain.projection;

import org.springframework.data.rest.core.config.Projection;
import ru.airplane.server.domain.user.User;

import java.util.Set;

@Projection(name = "userProjection", types = {User.class})
public interface UserProjection {
    Integer getId();
    String getName();
    Set<RoleProjection> getRoles();
}
