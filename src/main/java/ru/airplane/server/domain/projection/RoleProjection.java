package ru.airplane.server.domain.projection;

import org.springframework.data.rest.core.config.Projection;
import ru.airplane.server.domain.user.Role;

@Projection(name = "roleProjection", types = {Role.class})
public interface RoleProjection {
    Integer getId();
    String getName();
    String getRole();
}
