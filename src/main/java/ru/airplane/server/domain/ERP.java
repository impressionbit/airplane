package ru.airplane.server.domain;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import javax.persistence.*;

@Entity
@Table(name = "ERP")// электрорадиоизделия
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class ERP {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Integer id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "TYPE_OF_REPLACEMENT") // вид замены: обязательная или по дефектации
    private boolean typeOfReplacement;

    @ManyToOne
    @JoinColumn (name = "katalog_id")
    private Katalog katalog;
}
