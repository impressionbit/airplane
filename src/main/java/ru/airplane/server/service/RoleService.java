package ru.airplane.server.service;

import org.springframework.stereotype.Service;
import ru.airplane.server.domain.user.Role;
import ru.airplane.server.domain.repository.RoleRepository;

import java.util.Set;

@Service
public class RoleService {

    private final RoleRepository roleRepository;
    public RoleService(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    public void saveRole (Role role){
        roleRepository.save(role);
    }

    public void saveAllRole (Set<Role> roles){
        roleRepository.saveAll(roles);
    }
}
