package ru.airplane.server.service;

import org.springframework.stereotype.Service;
import ru.airplane.server.domain.user.User;
import ru.airplane.server.domain.repository.UserRepository;

import java.util.Optional;
import java.util.Set;

@Service
public class UserService {

    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }


    public void saveUser (User user){
        userRepository.save(user);
    }

    public User findUserById (long id){
        Optional<User> optionalUser = userRepository.findById(id);
        return optionalUser.orElse(null);
    }

    public <T> T findById (Class<T> type, long id){
        return userRepository.findAllById(type, id);
    }

    public <T> Set<T> findUsersByName (Class<T> type, String name){
        return userRepository.findAllByName(type, name);
    }

    public void deleteUser (User user){
        userRepository.delete(user);
    }

    public void deleteUserById (long id){
        userRepository.deleteById(id);
    }
}
