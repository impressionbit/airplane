package ru.airplane.server.service;

import org.springframework.stereotype.Service;
import ru.airplane.server.domain.user.Department;
import ru.airplane.server.domain.repository.DepartmentRepository;

@Service
public class DepartmentService {

    private final DepartmentRepository departmentRepository;

    public DepartmentService(DepartmentRepository departmentRepository) {
        this.departmentRepository = departmentRepository;
    }

    public void saveDepartment (Department department){
        departmentRepository.save(department);
    }


}
