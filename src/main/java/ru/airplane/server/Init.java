package ru.airplane.server;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
import ru.airplane.server.domain.repository.UserRepository;
import ru.airplane.server.domain.user.Department;
import ru.airplane.server.domain.user.Passport;
import ru.airplane.server.domain.user.Role;
import ru.airplane.server.domain.user.User;
import ru.airplane.server.domain.projection.UserProjection;
import ru.airplane.server.service.DepartmentService;
import ru.airplane.server.service.PassportService;
import ru.airplane.server.service.RoleService;
import ru.airplane.server.service.UserService;

import java.util.HashSet;
import java.util.Set;

@Component
public class Init implements ApplicationListener<ContextRefreshedEvent> {

    @Autowired
    UserService userService;

    @Autowired
    RoleService roleService;

    @Autowired
    PassportService passportService;

    @Autowired
    DepartmentService departmentService;

    /*
     * Метод вызывается при старте приложения
     * Можно использовать для первичной инициализации данных
     * И тестов
     */
    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {

        Role roleDirectorNew = new Role("ROLE_DIRECTOR", "DirectorNew");
        roleService.saveRole(roleDirectorNew);


        // Создаем новые роли через конструкторы
        Role roleEngineer = new Role("ROLE_ENGINEER", "Engineer");
        Role roleDirector = new Role("ROLE_DIRECTOR", "Director");
        Role roleAdmin = new Role("ROLE_ADMIN", "Admin");

        // Добавляем их в массив
        Set<Role> roles = new HashSet<>();

        roles.add(roleEngineer);
        roles.add(roleAdmin);

    }
}
